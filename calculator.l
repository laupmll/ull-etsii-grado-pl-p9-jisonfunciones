/* description: Parses end executes mathematical expressions. */

/* lexical grammar */


%start expressions
%{
	var reserved_words = {IF: 'IF', THEN: 'THEN', ELSE: 'ELSE', PI: 'PI', E: 'E', DEF: 'DEF'};
	
	function idORrw (x) {	
	return ((x.toUpperCase() in reserved_words)? x.toUpperCase() : 'ID');
	}
	
%}



%%

\s+                   /* skip whitespace */
[0-9]+("."[0-9]+)?\b  return 'NUMBER'
";"                   return ';'
\b[a-zA-Z_]\w*\b	return idORrw(yytext)

[=!]"=="		return 'OPERATOR3'
[<>=!]"="		return 'OPERATOR2'
[-+*/^%()<>=!]		return yytext
[,{}]			return yytext

<<EOF>>               return 'EOF'
.                     return 'INVALID'




