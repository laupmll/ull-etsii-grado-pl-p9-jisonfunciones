/* description: Parses end executes mathematical expressions. */


/* operator associations and precedence */

%right '=' '<' '>'
%left '<=' '>=' '==' '!='
%left 'OPERATOR2'
%left 'OPERATOR3'
%left '+' '-'
%left '*' '/'
%left '^'
%right '!'
%right '%'
%right EL
%left ELSE
%left UMINUS
%right IDPARAM

%start expressions

%{
	
	var tab = '\t';
	var symbol_table = [];
	var indice_symbol_table = -1;  // índice del vector por el q vamos 

	// Devuelve un objeto hash.
	// Recibe el nombre de la función 
	//  y un array con el número de 
	//  parámetros que tiene.
	var symbol = function(name, params) {
		return {
			name: name,
			params: params
		};
	};
	
	// Devuelve el número de parámetros de la función pasada
	//  por parámetro.
	function numParams (function_name) {
		for (var i in symbol_table){
			if (symbol_table[i].name == function_name){
				//alert("encontrada fun en "+i+"params:"+symbol_table[i].params);
				return symbol_table[i].params.length;
			}
		}	
		return null;
	}

	/* if x then y */
	/*$$=[]; $$.push($2); $$.push('jmpz'); $$.push($4); $$.push(':endif');*/
	function translateIf (x, y){
		var pila = [];
		pila.push(x);
		//pila.push('jmpz');
		pila.push(tab+'jmp');
		pila.push(y);
		//pila.push(':endif');
		return (pila);
	}
	
    	/*IF x THEN y ELSE z */
	/*$$=[]; $$.push($2); $$.push('jmpz'); $$.push($4); $$.push('jmp'); 
	$$.push(':else'); $$.push($6); $$.push(':endif');*/
	function translateIfElse (x, y, z){
		var pila = translateIf(x, y);
		//alert('traslateIf'+pila);
		pila.push(tab+'jmp');
		pila.push(':else');
		pila.push(z);
		pila.push(':endif');
		return pila;
	}	

	/*Doble operador*/
   	/*Ej:  e '+' e */
	/*$$=[]; $$.push('\t'+$3); $$.push('\t'+$1); $$.push('\t'+$2);*/
	function translateDoubleOp(x, operator, z){
		var pila = [];
		pila.push(x);
		pila.push(z);
		pila.push(tab+operator);
		return pila;
	}

		
	var array = function(hash){
		var vector = [];
		for (var k in hash){
			vector.push(hash[k]);
		}
		return vector;
	}
	

	var make_traverse = function(){
		var seen = [];
		return function(key, val) {
			if (typeof val == "object"){
				if (seen.indexOf(val)>=0){
					return undefined;
					seen.push(val);
				}
				return val};
		}
	};
	
%}

%% /* language grammar */

expressions
    : decs statements EOF
        { 
	  typeof console !== 'undefined' ? console.log($1) : print($1);
	//$$=[];
	//$$.push(5);
	$$ = [$1]; $$.push(':main'); $$.push($2);
return ('<br>'+'<pre>'+$$.toString()).split(',').join('<br>')+'</pre>'; //split pasa a vector
//return '<br>'+$1.join('<br>');  
//return '<ol>\n'+$1+'</ol>';  
	//return $1;
	}
    ;

decs 
    : /* */	{$$ = [];}
    | decs dec	{$$ = $1; $$.push($2);}
    ;

dec : DEF functionname optparameters '{' statements '}'
	{//alert("optparameters: "+$3);
	$$ = [':'+$2+tab+$3]; if($5.length != 0){$$.push($5);} $$.push('\treturn');} 
    ;

functionname
    : ID
	{$$ = [$1];

	var symb = symbol($1, []);
	symbol_table.push(symb);
	indice_symbol_table++;
	}
    ;

optparameters 
    : parameters1
	{$$ = [$1];}
    | '(' parameters1 ')'
	{$$ = [$2];}
    ;

parameters1
    : /**/	{$$ = [];}
    | parameters
	{$$ = ['args\t'+$1];}
    ;

parameters
    : parameters ',' ID
	{
	$$ = $1+' :'+$3;
	symbol_table[indice_symbol_table].params.push($3);
	}
    | ID		%prec IDPARAM
	{$$ = ':'+$1;
	symbol_table[indice_symbol_table].params.push($1);
	}
    ;

statements   
    : statements ';' s			/*Permite varias sentencias*/	
	{	$$= $1; 
		//alert("$3: "+$3.length);
		if ($3.length != 0){
			$$.push($3);
		}
	}
    | s		{$$ = $1;}
    ;

s   :		{$$ = [];}	/*Permite que hayan sentencias vacías*/ 
    | e		{$$ = [$1];}
    | IF e THEN s	%prec EL	
	{$$ = translateIf($2, $4); $$.push(':endif'); }  
    | IF e THEN s ELSE s
	{$$ = translateIfElse($2, $4, $6); }
    ;


e
    : ID '=' e
	{ $$=[]; $$.push($3); $$.push(tab+'&'+$1); $$.push(tab+$2);}
    | ID
	{$$ = [tab+'$'+$1]; }
    | ID '(' oplista ')'
	{// Llamada a función
	$$ = $3; $$.push('\tcall :'+ $1); 
	
	var numParsLlamada = $3.length -1;
	var numPars = numParams($1);
	if (numParsLlamada !== numPars){
		throw new Error('Número de argumentos: '+numParsLlamada+' inválido en la llamada a '+$1+'. Deben ser '+numPars); 
	}
	}
    | e '+' e
        { $$ = translateDoubleOp($1, $2, $3); }
    | e '-' e
        { $$ = translateDoubleOp($1, $2, $3); }
    | e '*' e
        { $$ = translateDoubleOp($1, $2, $3); }
    | e '/' e
        { $$ = translateDoubleOp($1, $2, $3); 
	if ($3 === 0){ throw new Error('Division by zero');}}
    | e '^' e
        { $$ = translateDoubleOp($1, $2, $3); }
    | '!' e
        {$$ = []; $$.push(tab+$1); $$.push($2);}
    | e '%' e
        { $$ = translateDoubleOp($1, $2, $3);} 
    | '-' e %prec UMINUS
        {$$ = []; $$.push($2); $$.push(tab+$1);}
    | '(' e ')'
        {$$ = [$2];}
    | NUMBER
        {$$ = [tab+Number(yytext)];}
    | E
        {$$ = [tab+Math.E];}
    | PI
        {$$ = [tab+Math.PI];}
    | E '=' e
	{throw new Error('E can not be assigned'); }
    | PI '=' e
	{throw new Error('PI can not be assigned'); }
    | e 'OPERATOR2' e
        {/*alert("yytext: "+$2);*/
         $$ = translateDoubleOp($1, $2, $3); }
    | e 'OPERATOR3' e
        { $$ = translateDoubleOp($1, $2, $3); }
    | e '<' e 
        { $$ = translateDoubleOp($1, $2, $3); }
    | e '>' e 
        { $$ = translateDoubleOp($1, $2, $3); }
    ;

oplista
    : /* */ {$$ = [];}
    | lista {$$ = $1;}
    ;
 
lista
    : e { $$ = [$1];}
    | lista ',' e { $$ = $1; $$.push($3);}
    ;
